import logging

import requests
from util import Util as ut
import os
from datetime import datetime

import api_constants as API

u = ut()


class ApiClient:
    """
    Each request to the API requires authentication.
    Append authentication to all requests.
    """
    def append_payload_authentication(self, payload, auth_type='api'):
        payload.update({
            'auth_type': auth_type
            , 'token': self.token
            , 'username': self.username
        })
        return payload

    """
    Send off a 'get' API request and automatically set authentication.
    """
    def get(self, method, params, catch_errors=False):
        try:
            params = self.append_payload_authentication(params)
            res = requests.get(self.url + '/' + method, params=params)
            return res
        except Exception as e:
            if catch_errors:
                return None
            raise Exception(e)

    """
    Refer to above.
    """
    def post(self, method, params={}, files=None, catch_errors=False):
        try:
            params = self.append_payload_authentication(params)
            res = requests.post(self.url + '/' + method, files=files, params=params)
            return res
        except Exception as e:
            if catch_errors:
                return None
            raise Exception(e)

    """
    Is this device even registered with the backend?
    """
    def confirm_device_id(self, device_id):
        try:
            res = self.get(API.DEVICES, {'id': device_id})
            if res.status_code != 200: return False
        except Exception as e:
            return False
        return True

    """
    Register a new device on the server.
    """
    def register_device(self, device_name):
        try:
            res = self.post(method=API.DEVICES, params={'name': device_name})
            if u.dotRead(res.json(), 'inserted') == '0':
                raise Exception("Device registration unsuccessful...")
            device_id = u.dotRead(res.json(), 'generated_keys')[0]
        except Exception as e:
            print(e)
            return None

        return device_id

    """
    Upload a device registered file to the server
    """
    def upload_file(self, device_id, file=None, stream=None, ext='.jpg', key='image', endpoint=API.INCIDENTS):
        res = None

        files = {}
        filename = datetime.now().strftime('%Y-%m-%d_%H%M-%S-%f')

        # uploading from file, load into memory and get ready for upload.
        if file:
            filename = filename + os.path.splitext(file.name)[1]
            try:
                with open(file, 'rb') as file:
                    files[key] = file
            except IOError as e:
                return None

        # uploading from a stream, set required values instead.
        if stream:
            filename = filename + ext
            files[key] = stream.getvalue()

        # send off the request.
        res = self.post(endpoint, files=files, params={
            'device_id': device_id,
            'date': datetime.now().strftime('%Y-%m-%d'),
            'filename': filename
        })

        if res.status_code != 200:
            raise Exception(
                "Abnormal server response (%i: %s), aborting." % (res.status_code, u.dotRead(res.json(), 'message')))

        if u.dotRead(res.json(), 'inserted') == '0':
            raise Exception("Failed to upload image: (%s)" % u.dotRead(res.json(), 'message'))

        # close resources after use.
        if file:
            file.close()
        if stream:
            stream.close()

        return res

    def __init__(self, username, token, url):
        self.logger = logging.getLogger(__name__)

        self.username = username
        self.token = token
        self.url = url
