# Installation on Raspberry Pi 1

1. Flash the device; in this instance we installed Jessie-Lite 2017-03-02 to a 16Gb Lexar microSD card.
2. Boot up the device, login as pi:raspberry and enter `sudo raspi-config` to configure the device.
    1. Configure the hostname.
    2. Enable the camera
    3. Set internationalisation options; in this case the keyboard is set to English US
    4. Enable SSH
    5. Optional: I overclocked to 800Mhz
    6. Reset the `pi` user's password
    7. Set the timezone
3. Reset the device and login
4. Now, we'll need internet access to install necessary software, either connect to Ethernet or follow the Wifi setup instructions.
5. Install necessary components `sudo apt-get update && sudo apt-get -y install git python3`
6. `mkdir code && cd code && python3 -m venv env-seclient && source env-seclient/bin/activate`
    1. If you have trouble with `venv` try [this link](http://stackoverflow.com/questions/24123150/pyvenv-3-4-returned-non-zero-exit-status-1)
    2. `sudo apt-get install -y python-virtualenv`
    3. `virtualenv --python=/usr/bin/python3.4 env-seclient`
    4. `source env-seclient/bin/activate`
7. `git clone https://gitlab.com/razodactyl/seclient.git && cd seclient`
8. `cp motion_example.json motion.json`
9. Fill out details for `nano motion.json`
10. Before installing Pillow via requirements.txt, some libraries will need to be installed which can be found at [this link](http://pillow.readthedocs.io/en/latest/installation.html#building-on-linux)
    1. If you have issues installing certain libraries, try installing them one by one.
11. `pip install -r requirements.txt`

## Wifi setup instructions
1. The following files are our focus
    1. `/etc/network/interfaces` configures the available interfaces
    2. `/etc/wpa_supplicant/wpa_supplicant.conf` configures the Wifi credentials
2. Follow the guide [here](http://weworkweplay.com/play/automatically-connect-a-raspberry-pi-to-a-wifi-network/)

## Auto-start / auto-reconnect configuration
1. Auto-start seclient via `start.sh`
    1. `crontab -e`
    2. `@reboot bash /home/pi/code/seclient/start.sh >/home/pi/logs/cronlog 2>&1`
2. Automatically reconnect Wifi connection
    1. `cd ~/code/seclient`
    2. `sudo cp scripts/wifi_reconnect.sh /usr/local/bin/wifi_reconnect.sh && sudo chmod +x /usr/local/bin/wifi_reconnect.sh`
    3. `sudo crontab -e`
    4. `*/1 * * * * /usr/local/bin/wifi_reconnect.sh`

## Disable camera LED (optional)
http://www.raspberrypi-spy.co.uk/2013/05/how-to-disable-the-red-led-on-the-pi-camera-module/
