
import json
import os
from util import Util as ut
u = ut()

class Configuration():
    def __init__(self, config_file, catch_errors=True):
        self.filename = os.path.basename(config_file)

        try:
            with open(config_file) as config:
                self.configuration = json.load(config)
        except IOError as e:
            if catch_errors:
                raise IOError(e)
            else:
                # allow manual handling.
                # in this instance we can continue with a blank configuration.
                # this allows fabricating a configuration and saving the file.
                self.configuration = {}

    def set(self, option, value):
        self.configuration[option] = value;

    def get(self, option=None, default=None):
        # a specific option was requested
        if option:
            return u.dotRead(self.configuration, option, default)
        # no option, return the whole configuration
        return self.configuration

    # allow configuration to be written back to the file
    def save(self):
        with open(self.filename, 'w') as f:
            f.write(json.dumps(self.get()) + "\n")
