#!/bin/bash

ENV_DIR=$(eval echo ~${SUDO_USER})"/env-seclient/bin"
APP_DIR=$(eval echo ~${SUDO_USER})"/seclient"

start_motion()
{
    echo 'Creating surveillance process:'
    echo 'stdout -> motion.log'
    echo 'stderr -> motion_err.log'
    # it's necessary to cd to the APP_DIR
    # motion.py requires relative files like motion.conf
    cd $APP_DIR
    python motion.py > /dev/null 2> /dev/null &
}

echo 'Activating required environment...'

if [ -d "$ENV_DIR" ]; then
    echo 'Environment exists, activate it.'
    source $ENV_DIR/activate

    start_motion
else
    echo 'Error: client virtualenv missing!'
    echo $ENV_DIR
    exit 1
fi

echo 'Complete!'
