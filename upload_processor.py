
import logging

import time
import os
from datetime import datetime

import threading
from api_client import ApiClient

class UploadProcessor(threading.Thread):
    def __init__(self, lock, pool):
        self.logger = logging.getLogger(__name__)

        super(UploadProcessor, self).__init__()
        self.lock = lock
        self.pool = pool

        self.fallback_dir = None

        self.stream = None
        self.username = None
        self.token = None
        self.url = None
        self.device_id = None

        self.event = threading.Event()
        self.terminated = False
        self.start()

    def save_image(self, stream=None, dir="/home/pi/capture"):
        try:
            if stream and self.fallback_dir:
                # If it doesn't exist, recursively create it.
                if not os.path.exists(self.fallback_dir):
                    os.makedirs(self.fallback_dir)
                # The full path including the filename.
                filename = os.path.join(self.fallback_dir, datetime.now().strftime('%Y-%m-%d_%H%M-%S-%f') + ".jpg")
                with open(filename, 'wb') as file:
                    file.write(stream.getvalue())
                self.logger.info("Saving image to '%s'" % filename)
            else:
                self.logger.critical("No stream provided or fallback dir not provided for upload processor.")
        except Exception as e:
            self.logger.critical(type(e), "Failed to save image to fallback directory.")

    def run(self):
        while not self.terminated:
            if self.event.wait(1):
                try:
                    self.logger.debug("An upload processor is running...")
                    if self.stream:
                        # Count how many retries so far.
                        retry = 0
                        # Flag when the upload completed successfully.
                        success = False
                        # Maxiumum amount of exceptions handled before giving up.
                        MAX_RETRY = 3
                        while (retry < MAX_RETRY):
                            try:
                                client = ApiClient(self.username, self.token, self.url)
                                client.upload_file(self.device_id, stream=self.stream, ext='.jpg')
                                retry = MAX_RETRY
                                success = True
                            except Exception as e:
                                self.logger.error(e)
                                retry += 1

                        # Fallback recovery, this is bad - save the image to another location.
                        if not success:
                            self.logger.error("Failed to report an image...")
                            self.save_image(stream=self.stream, dir=self.fallback_dir)
                finally:
                    self.event.clear()
                    with self.lock:
                        self.logger.debug("Upload processor returned to the pool.")
                        self.pool.append(self)
