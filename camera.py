
import logging

import picamera
from PIL import Image
import io

import time
from fractions import Fraction
from util import Util as ut
u = ut()

class Camera():
    def set_from_options(self, options):
        self.camera.hflip = u.dotRead(options, 'hflip', False)
        self.camera.vflip = u.dotRead(options, 'vflip', False)
        # set supplementary options
        self.options = {
            "use_video_port": u.dotRead(options, 'use_video_port', False)
        }

    def __init__(self, resolution=(640, 480), scratch=(100, 75), quality=80, options={}):
        self.logger = logging.getLogger(__name__)

        self.quality = quality

        self.scratch_width, self.scratch_height = scratch

        self.camera = picamera.PiCamera()
        self.camera.resolution = resolution
        self.set_from_options(options)

        self.logger.debug("Initialised camera module.")

        # playing around with fixed settings
        # let's capture in low light
        # self.camera.framerate = Fraction(1, 6)
        # self.camera.shutter_speed = 6000000 #6 seconds
        # self.camera.exposure_mode = 'off'
        # self.camera.iso = 800
        # time.sleep(10)

    # Deals with camera hardware and configuration, returns (stream, buffer)
    def capture_image(self, resize=None, quality=None, use_video_port=None):
        stream = io.BytesIO()

        # If quality isn't specified, falls back to internal setting.
        if quality == None:
            quality = self.quality

        # If not explicitly defined, revert to default.
        if use_video_port == None:
            use_video_port = u.dotRead(self.options, 'use_video_port')

        self.camera.capture(stream, format='jpeg', resize=resize, quality=quality, use_video_port=use_video_port)

        stream.seek(0)

        try:
            im = Image.open(stream)
            buffer = im.load()
            im.close()
            stream.seek(0)
        except Exception as e:
            self.logger.error(type(e), e)
            return None
        return stream, buffer

    # Motion detection doesn't work well with artifacting, snaps the test image at 100 quality.
    def capture_test_image(self):
        return self.capture_image(resize=(self.scratch_width, self.scratch_height), quality=100, use_video_port=False)[1]

    def close(self):
        self.logger.info("Closing camera module.")
        self.camera.close()
