
import logging
import logging.config

import time
import threading
import os

from configuration import Configuration
from api_client import ApiClient
from camera import Camera
from upload_processor import UploadProcessor

from util import Util as ut
u = ut()

class Motion():
    def __init__(self, motion_conf='motion.json', id_conf='id.json'):
        self.logger = logging.getLogger("motion")

        self.config = Configuration(motion_conf)

        quality = self.config.get('quality', 80)
        options = self.config.get('camera_options', {})

        save_width = self.config.get('save_width', 1296)
        save_height = self.config.get('save_height', 972)

        scratch_width = self.config.get('scratch_width', 100)
        scratch_height = self.config.get('scratch_height', 75)

        self.camera = Camera(resolution=(save_width, save_height), scratch=(scratch_width, scratch_height), quality=quality, options=options)

        # When the main image uploader threads can't keep up, buffer excess frames here.
        # They are popped off the queue by uploaders when more become available.
        self.capture_stream_queue = []
        self.capture_stream_queue_max = 80

        self.threshold = self.config.get('threshold', 30)
        self.sensitivity = self.config.get('sensitivity', 30)

        self.buffer = self.camera.capture_test_image()

    def detect_motion(self, auto_append=True):
        #self.logger.debug("Detecting motion...")

        changed_pixels = 0

        buffer = self.camera.capture_test_image()

        motion_detected = False

        max_pixdiff = 0

        if buffer:
            for x in range(0, self.camera.scratch_width):
                for y in range(0, self.camera.scratch_height):
                    pixdiff = abs(self.buffer[x,y][1] - buffer[x,y][1])

                    # Statistics, we want to know how far these pixels are changing
                    if (pixdiff > max_pixdiff):
                        max_pixdiff = pixdiff

                    if pixdiff > self.threshold:
                        changed_pixels += 1

                    if changed_pixels > self.sensitivity:
                        # motion detected, capture image and break from vertical loop
                        motion_detected = True
                        break
                # motion detected, break from horizontal loop
                if motion_detected:
                    break

        if motion_detected:
            self.logger.debug("Motion detected:")
            self.logger.debug("Ratio (max_pixdiff / threshold) = %s/%s" % (max_pixdiff, self.threshold))
            if auto_append:
                self.capture_to_stream_queue()

        # swap local and global buffer for next comparison
        self.buffer = buffer

        return motion_detected

    # Determine if any images were added to the stack of captured images.
    # Best use this to determine if any uploading needs to be done.
    # Remember to pop images off the stack when processed.
    def get_capture_queue_length(self):
        return len(self.capture_stream_queue)

    # Snaps a full-res picture and inserts left into the internal image queue.
    def capture_to_stream_queue(self):
        if (self.get_capture_queue_length() > self.capture_stream_queue_max):
            self.logger.critical("Stream queue is overloaded - discarded an image!")
            # Discard oldest image - this is very bad!
            self.pop_capture_stream_queue()

        (stream, buffer) = self.camera.capture_image()
        self.capture_stream_queue.insert(0, stream)
        self.logger.debug("Motion - Capture buffer length: (%i)" % len(self.capture_stream_queue))

    # Grabs the oldest image from the right of the internal image queue.
    def pop_capture_stream_queue(self):
        return self.capture_stream_queue.pop(0)

    def close(self):
        self.logger.info("Closing motion module.")
        self.camera.close()

def get_used_space(dir):
    if not dir:
        raise Exception("Can't get_used_space without dir parameter.")
    return sum(os.path.getsize(os.path.join(dir, f)) for f in os.listdir(dir) if os.path.isfile(os.path.join(dir, f))) / 1024 / 1024

def keep_disk_space_free(dir=None, fallback_space_to_reserve=8, filename_prefix='', extension='.jpg'):
    if not dir:
        raise Exception("Can't keep_disk_space_free without dir parameter.")

    # If dir doesn't exist, recursively create it.
    if not os.path.exists(dir):
        os.makedirs(dir)

    if (get_used_space(dir) > fallback_space_to_reserve):
        logging.info("%sMB / %sMB in use." % (get_used_space(dir), fallback_space_to_reserve))
        for filename in sorted(os.listdir(dir)):
            if filename.startswith(filename_prefix) and filename.endswith(extension):
                os.remove(os.path.join(dir, filename))
                logging.error('Deleted %s to avoid filling disk!' % filename)
                if (get_used_space(dir) < fallback_space_to_reserve):
                    return

def main():

    """
    Let's setup logging.
    Note: The 'logging.json->loggers' configuration corresponds to each item returned by 'logging.getLogger'
    we'll just setup root to handle everything but we could target specific modules if we wanted to by simply
    defining a logger targeting a specific module name.
    """
    try:
        # attempt to load from the config file
        logger_config = Configuration('logging.json')
        logging.config.dictConfig(logger_config.get())

        logging.info("\n\n+ ================================ +\n+ Logging initialised. \n+ ================================ +\n")
    except Exception as e:
        print("Error with logging:", type(e), e)
        quit()

    config = Configuration('motion.json')

    force_capture = config.get('force_capture')
    force_capture_time = config.get('force_capture_time')

    url = config.get('home_url')
    username = config.get('username')
    token = config.get('token')

    device_id = None
    api = ApiClient(username, token, url)
    try:
        id_config = Configuration('id.json', catch_errors=False)
        device_id = id_config.get('id')
        if not device_id or not api.confirm_device_id(device_id):
            logging.error("Device ID not registered.")
            raise Exception("Device ID not registered.")
    except Exception as e:
        logging.debug("Type: '%s', message: '%s'.", type(e), e)
        device_name = config.get('device_name')
        logging.info("Provisioning new device: %s." % device_name)
        device_id = api.register_device(device_name)
        id_config.set('id', device_id)
        id_config.save()
        logging.info("Registered new device: %s" % device_id)

    pool_lock = threading.Lock()
    pool = []

    m = None

    logging.info("Queuing upload processors.")

    # TODO: Upload processors consume memory, actively monitor memory and set an upper limit of how many processors can be running...
    # TODO: What if the server goes down? Network storage?
    for i in range(config.get('upload_processor_count', 15)):
        logging.debug("Adding upload processor %i." % i)
        pool.append(UploadProcessor(pool_lock, pool))

    logging.info("Starting motion detection loop.")

    fallback_dir = os.path.join(config.get('fallback_dir', '/home/pi/default_fallback_dir'), device_id)
    fallback_space_to_reserve = config.get('fallback_space_to_reserve', 16)

    try:
        m = Motion()
        running = True

        time_now = time.time()

        while running:
            # If motion was detected, or we've manually scheduled an image to be uploaded.
            if m.detect_motion() or m.get_capture_queue_length() > 0:
                with pool_lock:
                    if pool:
                        processor = pool.pop()
                    else:
                        processor = None
                if processor:
                    logging.debug("Acquired an upload processor.")

                    # load the processor with an image and required backend auth details.
                    processor.stream = m.pop_capture_stream_queue()
                    processor.username = username
                    processor.token = token
                    processor.url = url
                    processor.device_id = device_id
                    processor.fallback_dir = fallback_dir

                    # tell the processor to start working.
                    processor.event.set()
                else:
                    logging.info("Upload processor pool exhausted. Waiting for more uploaders...")

            # Do we want to capture images every N seconds?
            if (force_capture):
                # If timeout, let's capture an image to be processed.
                if ((time.time() - time_now) > force_capture_time):
                    # Reset the clock
                    time_now = time.time()
                    logging.info("Timeout reached. Capturing an image.")
                    m.capture_to_stream_queue()

            # Maintain fallback directory - don't use up more space than allocated
            keep_disk_space_free(dir=fallback_dir, fallback_space_to_reserve=fallback_space_to_reserve)

    except Exception as e:
        # TODO: Keep a proper log of errors and causes. We'll be logging this to the server and recovering the device by restarting it after a given timeout.
        logging.critical(type(e), e)
    finally:
        if m:
            m.close()

        logging.info("Terminating upload processors.")
        while pool:
            processor = pool.pop()
            processor.terminated = True
            processor.join()
            logging.debug("Upload processor terminated.")

    logging.info("Program ended.")

if __name__ == "__main__":
    main()    
#try:
    #    main()
    #except Exception as e:
    #    print("Unexpected exception:", e)
